<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('Quizmodel');
		$this->load->model('Msad');
	}

	public function index(){
		echo "xd";
	}

	public function extraerDatosParticipante(){
	  header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
      $participante = file_get_contents('php://input');
      $data = json_decode($participante, true);


      $respuesta = $this->Quizmodel->insertarParticipante($data);
      
      if($respuesta == true){
      	echo json_encode($data);
      }
      else{
      	echo json_encode(array("error" => "No se ha logrado registrar el participante"));
      }

	}

	public function traerPreguntas(){
	    header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    

		$pregunta = $this->Quizmodel->consultarPregunta();

		$juan = 0;

		foreach ($pregunta as $key => $value) {


			foreach ($value as $val) {

				$indices = array_keys($value);

					for ($i=1; $i <= count($indices); $i++) {

						if(preg_match("/".$indices[$juan]."/", 'option'.$i)){


							$options['option'.$i] = $val;

							unset($pregunta[$key]['option'.$i]);

							$pregunta[$key]['options'] = $options;
						} 

					}
						$juan++;
				
			}

			$juan = 0;
			

		} // fin foreach
	

		echo json_encode($pregunta);
	}

	public function respuestas(){
	  header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
      $id = file_get_contents('php://input');
      $id_pregunta = json_decode($id, true);

      foreach ($id_pregunta as $id ){

      	$respuesta[$id-1]['respuesta'] = $this->Quizmodel->consultarRespuestas($id);

      }

      echo json_encode($respuesta);

	}
}