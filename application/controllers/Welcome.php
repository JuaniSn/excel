<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;

use Box\Spout\Common\Type;

use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;

use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Msad');
        $this->load->database();
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}




    public function traerDatos(){
      $actual = date('d/m/Y');
      $a = $this->diff($actual, '30/10/2018');
      echo $a;

    }

    function diff($fecha1, $fecha2){

      $destruida = explode("/", $fecha2);

      $dia = $destruida[0];
      $mes = $destruida[1];
      $ano = $destruida[2];

      $destruida2 = explode("/", $fecha1);

      $dia2 = $destruida2[0];
      $mes2 = $destruida2[1];
      $ano2 = $destruida2[2];

      $con = $mes.'/'.$dia.'/'.$ano;
      $con2 = $mes2.'/'.$dia2.'/'.$ano2;
    
       $daaa = new DateTime($con);
       $daaa2 = new DateTime($con2);
      //$newData = date_create($fecha1);
      //$newData2 = date_create($fecha2);
      //print_r($newData);die;
      $result = date_diff($daaa, $daaa2);
      return $result->format('%R%a días');
    }

    public function pruebaAngular(){
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
      $json = file_get_contents('php://input');
      $data = json_decode($json, true);


      $respuesta = $this->calcularSituacion($data['edad'], $data['peso'], $data['talla'], $data['genero'], $data['embarazo'], $data['meses'], $data['cbi'], $data['complicacion_clinica']);
      echo json_encode($respuesta);
    }

     public function pruebaAngula2r($edad, $a){
      header("Access-Control-Allow-Origin: *");
      header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
      // sleep(10);

      echo $a;

    }

    public function calcularSituacion($edad, $peso, $talla, $genero, $embarazo, $meses, $cbi, $complicacion_clinica){
       $v_nutricional = json_decode(file_get_contents('/var/www/excel/application/core/vulnerabilidad_nutricional.json'),true);
       $imc = number_format($peso/($talla*$talla),1);
       $error = "";

       if($edad < 1 && $meses < 3){

        $i = "meses";
        $rangos = $v_nutricional[$i][$meses];

       }
       else if ($edad > 0 && $edad < 5){

        $i = "<5";
        $rangos = $v_nutricional[$i];

       }
       else if($edad >= 5 && $edad <= 9){

          $talla = $talla * 100;

          if($talla >= 55 && $talla <= 145){
            $i = "5-9";
            $rangos = $v_nutricional[$i][$talla];
          }
          else{
            $error = "Talla fuera de rango";
          }

       }
       else if($edad >= 10 && $embarazo == 1){

        $i = "embarazada";
        $rangos = $v_nutricional[$i];

       }
       else if($edad >= 10){

        $i = ">10";
        $rangos = $v_nutricional[$i]["IMC"];
        $rangosCbi = $v_nutricional[$i]["CBI"];

       }

       if($error == ""){

           foreach ($rangos as $key => $value) {

            $rangoMinimo = $rangos[$key][0]; 
            $rangoMaximo = $rangos[$key][1];

               if($edad < 1 && $meses < 3){
                    if($peso <= $rangoMaximo && $peso >= $rangoMinimo){
                        $respuesta = $key;
                    }
               }
               else if($edad > 0 && $edad < 5){
                    if($cbi <= $rangoMaximo && $cbi >= $rangoMinimo){
                        $respuesta = $key;
                    }
               }
               else if($edad >= 5 && $edad <= 9){
                    if($peso <= $rangoMaximo && $peso >= $rangoMinimo){
                        $respuesta = $key;
                    }
               }
               else if($edad >= 10 && $embarazo == 1){
                   if($cbi >= $rangoMinimo && $cbi <= $rangoMaximo){
                        $respuesta = $key;
                   } 
               }
               else if($edad >= 10){
                    $rangoMinimoCbi = $rangosCbi[$key][0];
                    $rangoMaximoCbi = $rangosCbi[$key][1];

                    if($cbi >= $rangoMinimoCbi && $cbi <= $rangoMaximoCbi){
                        $respuesta = $key;
                    }
                    if($imc >= $rangoMinimo && $imc <= $rangoMaximo){
                        $respuesta2 = $key;
                    }

               }
              
           }


           if(isset($respuesta2)){
                if($respuesta != 'A' || $respuesta2 != 'A'){
                    $respuesta = 'B';
                }
           }

           if($respuesta == 'B' && $complicacion_clinica == '1'){
                $respuesta = 'C';
           }


         return $respuesta;
       }
       else{
        return $error;
       }
    }


    public function calcularIndices($edad="5",$peso="15",$talla="1.2",$genero="1"){
        //prp(gettype($edad),1);
        $tablaImc = json_decode(file_get_contents('/var/www/evaluacion_apg/data/tabla_imc.json'),true);
        $leyenda = $tablaImc['leyenda'];        
        $imc =  number_format($peso/($talla*$talla),1);
        $imc = 50.00;
        $respuesta="";
        $codigo="";
        $descripcion = "";
        if($talla>0 && $peso>0){
            if ($edad<10){
                $indice="<10";
                $leyenda=$tablaImc["leyenda"]["001"];
            }else if (($edad>=19)&&($edad<=24)){
                $indice="19-24";
                $leyenda=$tablaImc["leyenda"]["003"];
            }else if (($edad>=25)&&($edad<=29)){
                $indice="25-29";
                $leyenda=$tablaImc["leyenda"]["003"];
            }else if (($edad>=30)&&($edad<=59)){
                $indice="30-59";
                $leyenda=$tablaImc["leyenda"]["003"];
            }else if (($edad>=60)&&($edad<=79)){
                $indice="60-79";
                $leyenda=$tablaImc["leyenda"]["003"];
            }else if ($edad>79){
                $indice=">79";
                $leyenda=$tablaImc["leyenda"]["003"];
            }else {
                $indice=$edad;
                $leyenda=$tablaImc["leyenda"]["002"];
            }
            //console.log(leyenda)
            if ($edad<10){
                $rangos=$tablaImc[$indice][$genero][($talla*100)];
            }else{
                $talla = $talla/100; //se convierte de centimetros  metros para sacar el imc
                $rangos=$tablaImc[$indice][$genero];

            }
            
            foreach ($rangos as $i => $valor) {
                                        

                $rangoMinimo = $rangos[$i][0];
                $rangoMaximo = $rangos[$i][1];
                if( $edad >= 10 ){
                    if(isset($rangos[$i-1])){
                        $rangoMinimo = $rangos[$i-1][1];

                    }
                    if(isset($rangos[$i+1])){
                        $rangoMaximo = $rangos[$i+1][1];

                    }                    
                    if( ($imc >= $rangoMinimo) && ($imc <= $rangoMaximo) ){
                        $respuesta = $i;
                    }
                }else{                  
                    if( ($peso > $rangoMinimo) && ($peso < $rangoMaximo) ){
                        $respuesta = $i;
                    }                   
                }                
            }            
            $codigo = $leyenda[$respuesta];    
        }

            $salida = array(
                'codigo'       => $codigo,
                'descripcion'  => $respuesta,
                'imc'          => $imc
            );
                print_r($salida);die;

            return $salida;

    }


    public function prueba(){
      $respuesta = $this->fecha();
      $array2 = array();

      foreach ($respuesta as $key => $value) {
        $array2[$key] = $respuesta[$key]['codigo_persona'];  
      }
     // echo $array2;
      $contador_original = count($array2);
      $el_otro = count(array_unique($array2));
      
      if($contador_original != $el_otro){
        $res = $contador_original - $el_otro;
        echo $res;
      }
      else{
        echo "no";
        echo "<br>";
        echo "El otro- ".$el_otro;
        echo "<br>";
        echo "Origin- ".$contador_original;
      }
 
/*
     if((count($respuesta))!=(count(array_unique($respuesta)){

     }
*/

    }

    public function fecha(){

      $fecha1 = '01/01/2016'.' 00:00:00';
      $fecha2 = '31/10/2018'.' 23:59:59';

      $SQL = "
            SELECT
                  resultado.codigo_persona
 
              FROM ( SELECT row_number() OVER (PARTITION BY p.codigo_persona ORDER BY r.fecha_registro) AS rn,
                    p.codigo_persona

             FROM vulnerabilidad.t_persona p

             LEFT JOIN vulnerabilidad.t_evaluacion te ON te.codigo_persona = p.codigo_persona
             LEFT JOIN vulnerabilidad.t_ubicacion_persona up ON up.codigo_persona = p.codigo_persona
             LEFT JOIN vulnerabilidad.t_registro_persona rp ON rp.codigo_evaluacion = te.codigo_evaluacion
             LEFT JOIN vulnerabilidad.t_registro r ON r.codigo_registro = rp.codigo_registro
             LEFT JOIN bms.t_estados e ON e.codigo_estado = up.codigo_estado
             LEFT JOIN bms.t_municipios m ON m.codigo_municipio = up.codigo_municipio AND e.codigo_estado = m.codigo_estado
             LEFT JOIN bms.t_parroquias pa ON pa.codigo_parroquia = up.codigo_parroquia AND e.codigo_estado = pa.codigo_estado AND m.codigo_municipio = pa.codigo_municipio
             LEFT JOIN vulnerabilidad.t_sector se ON se.codigo_sector = up.codigo_sector AND e.codigo_estado = se.codigo_estado AND m.codigo_municipio = se.codigo_municipio AND pa.codigo_parroquia = se.codigo_parroquia
             LEFT JOIN vulnerabilidad.t_origen o ON o.codigo_origen = te.codigo_origen
            
           
             ) resultado
             WHERE resultado.rn = 1;";

     
      $query = $this->db->query($SQL);
      $resultado = $query->result_array();
      return $resultado;
    }

    public function fecha2(){
      $a = array('codigo_persona' => 'PER2097');
      $this->db->select('*');
      $this->db->from('vulnerabilidad.v_reporte');
      $this->db->where($a);
      $resultado = $this->db->get();
      $resultado = $resultado->result_array();
      $this->Msad->prp($resultado,1);

    }

    public function guardar_ubicacion(){

        if(!empty($_FILES['file']['name'])){

                  $data = pathinfo($_FILES["file"]["name"]);

                  // print_r($data);    
                   if (($data['extension'] == 'xlsx' || $data['extension'] == 'xls') && $_FILES['file']['size'] > 0 ) {

                    $nameExcel= $_FILES['file']['tmp_name']; 
                    // esto lee el excel 
                    $reader = ReaderFactory::create(Type::XLSX);
                    $reader->setShouldFormatDates(true);

                    $reader->open($nameExcel);
                    $count = 1;

                    //Numero de Hojas en el Archivo
                    foreach ($reader->getSheetIterator() as $sheet) {

                        // Numero de filas en el documento EXCEL
                        foreach ($sheet->getRowIterator() as $row) {

                            // Lee los Datos despues del encabezado
                            // El encabezado se encuentra en la primera fila
                             if($count > 1) {

                                  $codigo_ubicacion[$count-1]   = $row[1];
                                  $codigo_estado[$count-1]      = $row[2];
                                  $codigo_municipio[$count-1]   = $row[4];
                                  $codigo_parroquia[$count-1]   = $row[6];
                                  $codigo_sector[$count-1]      = $row[9];
                                  $codigo_persona[$count-1]     = $row[10];
                                  $poblacion_indigena[$count-1] = $row[11];   
                            }
                        $count++;
                        }
                    }

                    $reader->close();
    
    foreach ($codigo_persona as $key => $value) {
        $consulta[$key] = $this->Msad->consulta_numero_sector($codigo_estado[$key], $codigo_municipio[$key], $codigo_parroquia[$key], $codigo_sector[$key]);

        $contador[$key] = (isset($consulta[$key])) ? $consulta[$key] : 0;

        $numero[$key] = $this->Msad->formato_ceros($contador[$key]+1,7);
        $contador[$key] ++;
        $numero_sector[$key] = $numero[$key];
        $numero_ubicacion[$key] = $contador[$key];
        $cedula_ubicacion[$key] =$codigo_estado[$key].$codigo_municipio[$key].$codigo_parroquia[$key].$codigo_sector[$key].$numero[$key];


        $ubicacion_persona[$key] = array('codigo_estado'             => $codigo_estado[$key],
                                         'codigo_municipio'          => $codigo_municipio[$key],
                                         'codigo_parroquia'          => $codigo_parroquia[$key],
                                         'codigo_sector'             => $codigo_sector[$key],
                                         'poblacion_indigena'        => $poblacion_indigena[$key],
                                         'numero_ubicacion'          => $numero_ubicacion[$key],
                                         'cedula_ubicacion'          => $cedula_ubicacion[$key],
                                         'codigo_persona'            => $codigo_persona[$key],
                                         'codigo_ubicacion_persona'  => $codigo_ubicacion[$key]);

        $this->Msad->guardar_ubicacion_persona($ubicacion_persona[$key]);

    }
        
            
                  
                     
                         
                            

          

                    }else{
                        echo "Selecciona un archivo valido";
                    }
                }else{
                    echo "Selecciona un archivo excel";
                }

    }   

    function actualizar_persona(){

            if(!empty($_FILES['file']['name'])){

                      $data = pathinfo($_FILES["file"]["name"]);

                       //print_r($_FILES);die;  
                       if (($data['extension'] == 'xlsx' || $data['extension'] == 'xls') && $_FILES['file']['size'] > 0 ) {

                        $nameExcel= $_FILES['file']['tmp_name']; 
                        // esto lee el excel 
                        $reader = ReaderFactory::create(Type::XLSX);
                        $reader->setShouldFormatDates(true);

                        $reader->open($nameExcel);
                        $count = 1;

                        //Numero de Hojas en el Archivo
                        foreach ($reader->getSheetIterator() as $sheet) {

                            // Numero de filas en el documento EXCEL
                            foreach ($sheet->getRowIterator() as $row) {

                                // Lee los Datos despues del encabezado
                                // El encabezado se encuentra en la primera fila
                                 if($count > 1) {

                                      $codigo_persona[$count-1]      = $row[0];
                                      $sector[$count-1]              = $row[2];
                                                         
                                }
                            $count++;
                            }
                        }

                        $reader->close();

                 $res = $this->Msad->updatePersonas($codigo_persona, $sector);

                 if($res == true){
                    echo "Actualizados";
                 }

                        //echo json_encode($arreglo);

                        }else{
                            echo "Selecciona un archivo valido";
                        }
                    }else{
                        echo "Selecciona un archivo excel";
                    }

    }
    function excelUbi($array, $filename) {
        header('Content-Disposition: attachment; filename='.$filename.'.xls');
        header('Content-type: application/force-download');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: public');
        print "\xEF\xBB\xBF"; // UTF-8 BOM

 
        echo "<table>";
        echo "<th>Cedula</th>";
        foreach($array as $key => $value){
                echo '<tr>'; 
           
                 echo '<td style="border:1px #888">'.$value.'</td>';  
            
        }
        echo '</tr>';
        echo '</table>';

    }

                         
    

  public function guardar_sectores(){
    //print_r($_FILES['file']['name']);
    if(!empty($_FILES['file']['name'])){

      $data = pathinfo($_FILES["file"]["name"]);

       //print_r($_FILES);die;  
       if (($data['extension'] == 'xlsx' || $data['extension'] == 'xls') && $_FILES['file']['size'] > 0 ) {

        $nameExcel= $_FILES['file']['tmp_name']; 
        // esto lee el excel 
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->setShouldFormatDates(true);

        $reader->open($nameExcel);
        $count = 1;

        //Numero de Hojas en el Archivo
        foreach ($reader->getSheetIterator() as $sheet) {

            // Numero de filas en el documento EXCEL
            foreach ($sheet->getRowIterator() as $row) {

                // Lee los Datos despues del encabezado
                // El encabezado se encuentra en la primera fila
               if($count > 1) {

                    $estado_1[$count-1]    = $row[0];
                    $estado[$count-1]    = $row[1];
                    $municipio_1[$count-1]   = $row[2];
                    $municipio[$count-1]   = $row[3];
                    $parroquia_1[$count-1]   = $row[4];
                    $parroquia[$count-1]   = $row[5];
                    $sector[$count-1]      = trim($row[6]);
                                   
          }
        $count++;
          }
      }

      $reader->close();
                $ubicaciones['estado_1'] = $estado_1;
                $ubicaciones['estado'] = $estado;
                $ubicaciones['municipio_1'] = $municipio_1;
                $ubicaciones['municipio'] = $municipio;
                $ubicaciones['parroquia_1'] = $parroquia_1;
                $ubicaciones['parroquia'] = $parroquia;
                $ubicaciones['sector'] = $sector;

     $this->space($ubicaciones, 'generado.xlsx');

      //echo json_encode($arreglo);

      }else{
        echo "Selecciona un archivo valido";
      }
    }else{
      echo "Selecciona un archivo excel";
    }
    }
    

    public function space($array, $nombreExcel){

	 $header_hoja1 = array();
        $header_hoja2 = ['Razón'];
        $data = array();
        $contador = 0;
        $row = 0;

         $data_kry = [
            ['Transcribe bien puto :v']
        ];
        foreach ($array as $key => $value) {

           array_push($header_hoja1, $key);

           foreach ($value as $indice) {
                $data[$row][$contador] = $indice;
                $row++;
           }
           
           $contador++;
           $row = 0;
        }

        $writer = WriterFactory::create(Type::XLSX);
        // $this->Msad->prp($writer);die;
                                                            
        $headerStyle = (new StyleBuilder())->setFontBold()
                                                                   ->setFontSize(15)
                                                                   ->setFontColor(Color::WHITE)
                                                                   ->setShouldWrapText(true)
                                                                   ->setBackgroundColor(Color::rgb(83,142,213))
                                                                   ->build();

        $writer->openToBrowser($nombreExcel); //descarga el excel

        $writer->getCurrentSheet()->setName('Tus errores feos');
        $writer->addRowWithStyle($header_hoja1, $headerStyle);
        $writer->addRows($data);

        $writer->addNewSheetAndMakeItCurrent()->setName('abre aqui xd');
        $writer->addRowWithStyle($header_hoja2, $headerStyle);
        $writer->addRows($data_kry);

        $writer->close();
    }




    public function crearSector(){
    	$estado = $this->input->post('estado');
    	$municipio = $this->input->post('municipio');
    	$parroquia = $this->input->post('parroquia');
    	$sector = $this->input->post('sector');

    	$sectores = $this->Msad->crearSector($estado, $municipio, $parroquia, $sector);

    	$this->generarExcel($sectores, 'codigo_de_sectores');
    }

    function generarExcel($array, $filename) {
        header('Content-Disposition: attachment; filename='.$filename.'.xls');
        header('Content-type: application/force-download');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: public');
        print "\xEF\xBB\xBF"; // UTF-8 BOM
        $h = array();
        foreach($array as $row){
            foreach($row as $key=>$val){
                if(!in_array($key, $h)){
                    $h[] = $key;   
                }
            }
        }
        echo '<table><tr>';
        foreach($h as $key) {
            echo '<th style="border:1px #888 solid;background:#025A98;color:white;">'.$key.'</th>';
            if($key == 'estado' || $key == 'municipio' || $key == 'parroquia' || $key == 'codigo_sector'){
            	 echo '<th style="border:1px #888 solid;background:#025A98;color:white;">'.$key.'_2'.'</th>';
            }
        }
        echo '</tr>';
 
        $iterador = 0;
        $letra = array(0 => 'A', 1 => 'C', 2 => 'E', 3 => 'G');
        $columnas = 0;
        $recorrerCeldas = 2;
        foreach($array as $row){
        	$columnas = count($array);
            	echo '<tr>';

            	
            foreach($row as $val){

            	if($val == 'existente'){
            		$color = '#D9E408';
            	}
            	else if($val == 'nuevo'){
            		$color = '#28D101';
            	}
            	else{
            		$color = 'rgb(238,236,225)';
            	}

            	if($iterador >= 0 && $iterador <= 3){
                	$this->writeRow($val, $color);
					$celda = $letra[$iterador].$recorrerCeldas;
					if($letra[$iterador] == 'G'){
						$this->writeRowCondicionalCeldaH($celda, $color);
					}
					else{
						$this->writeRowCondicional($celda, $color);
					}
	            }
            	else{
                	$this->writeRow($val, $color);
            	}
            	$iterador++;
            }
            $iterador = 0;
            $recorrerCeldas++;
        }
        echo '</tr>';
        echo '</table>';

    }

    function writeRow($val, $color) {
        echo '<td style="border:1px #888 solid;color:#555;background:'.$color.'">'.$val.'</td>';              
    }

    function writeRowCondicional($celda, $color){
    		echo '<td style="border:1px #888 solid;color:#555;background:'.$color.'">=SI('.$celda.'<9,CONCATENAR(0,'.$celda.'),'.$celda.')</td>';                  	
    }

    function writeRowCondicionalCeldaH($celda, $color){
    		echo '<td style="border:1px #888 solid;color:#555;background:'.$color.'">=SI('.$celda.'<10,CONCATENAR(0,0,'.$celda.'),CONCATENAR(0,'.$celda.'))</td>';
    		                  	
    }

    function generarExcelNombre($array, $filename) {
        header('Content-Disposition: attachment; filename='.$filename.'.xls');
        header('Content-type: application/force-download');
        header('Content-Transfer-Encoding: binary');
        header('Pragma: public');
        print "\xEF\xBB\xBF"; // UTF-8 BOM

        echo '<table><tr>';
            echo '<th>Nombre</th>';
        echo '</tr>';
 

        foreach($array as $row){

              echo '<tr>';

            foreach($row as $val){

                  $this->writeRow2($val);
              }

            

        }
        echo '</tr>';
        echo '</table>';

    }


     function writeRow2($val, $color) {
        echo '<td>'.$val.'</td>';              
    }

    

 	

}
