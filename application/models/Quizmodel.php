<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quizmodel extends CI_Model {


	function __construct(){
		parent::__construct();
	}

	public function insertarParticipante($data){
		$this->db->insert('public.participantes', $data);
		$afectadas = $this->db->affected_rows();

		if($afectadas > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function consultarPregunta(){
		$sql = "SELECT * FROM public.question";

		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;
	}

	public function consultarRespuestas($id_pregunta){
		$sql = "SELECT answer FROM public.question WHERE qnid = '{$id_pregunta}'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result[0]['answer'];
	}

	

}