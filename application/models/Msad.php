<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msad extends CI_Model {

	function __construct(){
		parent::__construct();
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');

	}

	public function index()
	{
		
	}

	public function crearSector($estado, $municipio, $parroquia, $sector){

		foreach ($estado as $key => $value) {	
			
			$codigo_sector[$key] = $this->consultarSector($estado[$key], $municipio[$key], $parroquia[$key], $sector[$key]);
			if($codigo_sector[$key] != ""){
				$existentes[$key] = array('estado' => $estado[$key],
										  'municipio' => $municipio[$key],
										  'parroquia' => $parroquia[$key],
										  'codigo_sector' => $codigo_sector[$key],
										  'sector'	=> $sector[$key],
										  'estatus' => 'existente'
										  );     
			}     

		 	if($codigo_sector[$key] == ""){

	          $codigo_sector[$key] = $this->formato_ceros($this->ultimoSector($estado[$key],$municipio[$key],$parroquia[$key]),3);
	          
	          $data[$key] = array(
	            'codigo_estado'       =>  strval($estado[$key]),
	            'codigo_municipio'    =>  strval($municipio[$key]),
	            'codigo_parroquia'    =>  $parroquia[$key],
	            'codigo_sector'       =>  $codigo_sector[$key],
	            'nombre_sector'       =>  $sector[$key]
	          );

	           $this->db->insert('vulnerabilidad.t_sector',$data[$key]);

	           $nuevos[$key] = array(
	            'estado'       =>  $estado[$key],
	            'municipio'    =>  $municipio[$key],
	            'parroquia'    =>  $parroquia[$key],
	            'codigo_sector'=>  $codigo_sector[$key],
	            'sector'       =>  $sector[$key]
	          );

			   $otro[$key] = array_merge($nuevos[$key], array('estatus' => 'nuevo')); 
	        }
		}

		if(isset($otro)){
			$super_arreglo = array_merge($existentes, $otro);
			return $super_arreglo;
		}else{
			return $existentes;
		}	
	}

	public function ultimoSector($codigo_estado="", $codigo_municipio="", $codigo_parroquia=""){
        $salida = 0;
        $condicion = array(
          'codigo_estado'       =>  strval($codigo_estado),
          'codigo_municipio'    =>  strval($codigo_municipio),
          'codigo_parroquia'    =>  strval($codigo_parroquia),
        );
        $this->db->select('id,codigo_sector');
        $this->db->from('vulnerabilidad.t_sector');
        $this->db->where($condicion);
        $rs = $this->db->get();
        //prp($this->db->last_query(),1);  
        $salida = count($rs->result_array())+1;
        $end = $rs->result_array();
        $end = end($end);

        if($end['codigo_sector'] == $salida){
        	$end = intval($end['codigo_sector']);
        	$end = $end + 1;
        	return $end;
        }

        return $salida;
    }

	function consultarSector($estado, $municipio, $parroquia, $sector){
			$condicion = array('codigo_estado' => strval($estado),
									 'codigo_municipio' => strval($municipio),
									 'codigo_parroquia' => strval($parroquia),
									 'nombre_sector'   => strval($sector));
	        $this->db->select('id,codigo_sector');
	        $this->db->from('vulnerabilidad.t_sector');
	        $this->db->where($condicion);
	        $rs = $this->db->get();
	        $rs = $rs->result_array();


	        if(count($rs)>0){
	          return $rs[0]['codigo_sector'];
	        }else{
	          return '';
	        }
	        return $salida; 
	}

	public function updatePersonas($codigo_persona, $sector){

		foreach ($codigo_persona as $key => $value) {
			$this->actualizarSector($codigo_persona[$key], $sector[$key]);
		}

		return true;
	}

	function actualizarSector($codigo_persona, $sector){
		
		$this->db->select('codigo_persona');
        $this->db->from('vulnerabilidad.t_persona');
        $this->db->where('codigo_persona',$codigo_persona);
        $rs = $this->db->get();

        $salida = $rs->result_array();
        if(!empty($salida)){

			$this->db->where('codigo_persona', $codigo_persona);
			$this->db->update('vulnerabilidad.t_ubicacion_persona', array('codigo_sector' => $sector));
        }

	}



	function prp($arg=array())
	{
		echo "\n<pre>\n";
		if( ! is_array($arg)) {
			if( ! is_object($arg)) {
				echo $arg;
			} else {
				print_r((array) $arg);
			}
		} else {
			print_r($arg);
		}
		echo "</pre>\n";
	}

	function formato_ceros( $numero, $ceros = 4 ){
		$salida = "";

		$cad_ceros = str_repeat( '0', $ceros );
		$salida = substr_replace( $cad_ceros, $numero , strlen( $cad_ceros ) - strlen( trim( (string) $numero ) ) );

		return $salida;
	}

	 public function consulta_numero_sector($codigo_estado="", $codigo_municipio="", $codigo_parroquia="", $codigo_sector=""){
      $this->db->select('numero_ubicacion');
      $this->db->from('vulnerabilidad.t_ubicacion_persona');      
      $this->db->where(array('codigo_estado' => strval($codigo_estado),
                              'codigo_municipio' => strval($codigo_municipio),
                              'codigo_parroquia' => strval($codigo_parroquia),
                              'codigo_sector'    => strval($codigo_sector)));
      $this->db->order_by("numero_ubicacion","desc");
      $rs = $this->db->get();
      //$this->prp($this->db->last_query(),1);die;
      //$this->prp($rs,1);die;
      $salida = $rs->row_array();
      $salida['numero_ubicacion'] = (isset($salida['numero_ubicacion'])) ? $salida['numero_ubicacion'] : 0;

      return $salida['numero_ubicacion'];
    }

    public function guardar_ubicacion_persona($ubicacion_persona){
    	//$this->prp($ubicacion_persona,1);
    
    	$this->db->select('*');
        $this->db->from('vulnerabilidad.t_persona');
        $this->db->where('codigo_persona',$ubicacion_persona['codigo_persona']);
        $rs = $this->db->get();

        $salida = $rs->result_array();

        if(empty($salida)){
          $this->db->insert('vulnerabilidad.t_ubicacion_persona', $ubicacion_persona);
        }
    	
     
    }






}